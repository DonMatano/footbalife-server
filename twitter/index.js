const Twitter = require("twitter-lite");
const TwitterConfig = require("../config/twitter");

module.exports = {
    initTwitterUser() {
        const client = new Twitter({
            subdomain: "api",
            consumer_key: TwitterConfig.consumerKey,
            consumer_secret: TwitterConfig.consumerSecret,
            access_token_key: TwitterConfig.accessTokenKey,
            access_token_secret: TwitterConfig.accessTokenSecret
        });
        return client;
    },

    async verifyTwitterUser(client) {
        try {
            const results = await client.get("account/verify_credentials")
            console.log(`Successfully Signed in ${results.name}`);
        } catch (err) {
            console.log(err)
        }
    },

    async getHomeTimeline(client, sinceTweetId) {
        const homeTimelineQuery = "statuses/home_timeline";
        try{
            if(!sinceTweetId){
                const tweets = await client.get(homeTimelineQuery, {
                    count: 5,
                    exclude_replies: true,
                    tweet_mode: 'extended'
                });
                console.log("Home TimeLine Gotten ");
                return tweets
            }
            else{
                const tweets = await client.get(homeTimelineQuery, {
                    exclude_replies: true,
                    since_id: sinceTweetId,
                    tweet_mode: 'extended'
                })
                console.log(`${tweets.length} New Tweets Gotten`);
                return tweets
            }
            
        } catch(err){
            console.log(err)
        }
    }
}


