var admin = require('firebase-admin');
var firebaseConfig = require('../config/firebase');
var serviceAccount = require("./Footbalife.json");
var firebaseDatabase = require('./database');
var firebaseFirestore = require('./firestore')

module.exports = {
    initialize(){
        admin.initializeApp({
            credential: admin.credential.cert(serviceAccount),
            databaseURL: firebaseConfig.credential.databaseURL
        })
    },

    getFireStoreDB(){
        const db = firebaseFirestore.getFirestore(admin)
        return db;
    },

    getDatabaseReference(){
        const databaseRef = firebaseDatabase.getDatabaseRef(admin)
        return databaseRef
    },

    setSinceTweetId(tweet){
        const sinceRef = this.getDatabaseReference().ref('since');
        sinceRef.set(tweet.id);
    },

    getSinceTweetId(){
        return this.getDatabaseReference().ref('/since').once('value').then(function (snapshot) {
            const sinceTweetId = (snapshot.val() || '');
            return sinceTweetId;
            // ...
        });
    },

    pushTweetsToDatabase(tweets){
        
        // const homeTweetRef = this.getDatabaseReference().ref('homeTweets')
        const collectionRef = this.getFireStoreDB().collection('homeTweets');

        console.log("Tweets Length", tweets.length);
        console.log("Tweets to be added", tweets.length !== 0)
        

        if(tweets.length !== 0){
            console.log(`About to add ${tweets.length} tweets`)
            tweets.forEach((tweet, index) => {
                if (index === 0) {
                    this.setSinceTweetId(tweet)
                }
                const tweetRef = collectionRef.doc(tweet.id.toString())
                tweetRef.set(tweet).then(ref => {
                    console.log(`Added tweet ${tweet.id} tweeted by ${tweet.user.screen_name}`)
                })
            });
        }
        else{
            console.log('Not Adding Tweets')
        }
        
    }
}


