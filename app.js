var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

var Twitter = require('./twitter/index');
var Firebase = require('./firebase/index')
const timeInterval = 90000;

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

Firebase.initialize();

const twitterUser = Twitter.initTwitterUser();
Twitter.verifyTwitterUser(twitterUser);

 function getAndSetTweet () {
    Firebase.getSinceTweetId()
        .then(sinceTweetId => {
                Twitter.getHomeTimeline(twitterUser, sinceTweetId)
                    .then(tweets => {
                        Firebase.pushTweetsToDatabase(tweets);
                    })
                    .catch(err => {
                        console.log(err);
                    });
            }
        );
}

getAndSetTweet();
setInterval(getAndSetTweet, timeInterval);

module.exports = app;
